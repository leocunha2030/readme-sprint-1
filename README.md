 Esse projeto é referente ao primeiro challenge proposto pelo nosso Scrum Master (SM) para a conclusão da Sprint 1.

<a class="gfm" href="https://gitlab.com/leocunha2030/readme-sprint-1/-/blob/main/Sprint%201/Dia1.md">Dia 1</a>
" - "
<strong> Primeiro dia. </strong>


<a class="gfm" href="https://gitlab.com/leocunha2030/readme-sprint-1/-/blob/main/Sprint%201/Dia2.md">Dia 2</a>
" - "
<strong> Aprendendo a metodologia Scrum. </strong>


<a class="gfm" href="https://gitlab.com/leocunha2030/readme-sprint-1/-/blob/main/Sprint%201/Dia3.md">Dia 3</a>
" - "
<strong> Aprimorando o conhecimento sobre Scrum. </strong>


<a class="gfm" href="https://gitlab.com/leocunha2030/readme-sprint-1/-/blob/main/Sprint%201/Dia4.md">Dia 4</a>
" - "
<strong> Conhecendo os fundamentos sobre Testes e QA. </strong>


<a class="gfm" href="https://gitlab.com/leocunha2030/readme-sprint-1/-/blob/main/Sprint%201/Dia5.md">Dia 5</a>
" - "
<strong> Aprimorando os fundamentos em testes de Software. </strong>


<a class="gfm" href="https://gitlab.com/leocunha2030/readme-sprint-1/-/blob/main/Sprint%201/Dia6.md">Dia 6</a>
" - "
<strong> Conhecendo as regras de Myers e os Princípios de Pareto </strong>

<a class="gfm" href="https://gitlab.com/leocunha2030/readme-sprint-1/-/blob/main/Sprint%201/Dia7&Dia8.md">Dia 7 e 8</a>
" - "
<strong> Aprendendo o basico de java </strong>

<a class="gfm" href="https://gitlab.com/leocunha2030/readme-sprint-1/-/blob/main/Sprint%201/Dia9.md">Dia 9</a>
" - "
<strong> Aprendendo o basico de Cyber Security </strong>
