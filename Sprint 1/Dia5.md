-----------------------------------------------------------------------------------
                --> Dia 5 <--
------------------------------------------------------------------------------------
--> Ciclo de desenvolvimento
------------------------------------------------------------------------------------
<h1>![imagem ciclo de desenvolvimento](https://github.com/leocunha2030/Projeto-Git/assets/134326959/9a3ef4ee-5fec-436c-95e5-ffd158726d53)</h1>
Conforme o projeto anda em questão de desenvolvimento, os testes também vão andando junto

--> Piramide de testes
------------------------------------------------------------------------------------
A função da pirâmide de testes é basicamente definir níveis de testes e te dar um norte quanto à quantidade de testes que você deveria ter em cada um desses níveis.
<h1>![piramide de testes](https://github.com/leocunha2030/Projeto-Git/assets/134326959/74c40f46-4fc7-47e9-aa05-0e9cf7035c86)</h1>

-> No topo da piramide temos os testes de ponta(e2e ou End to End)O objetivo deles é imitar o comportamento do usuário final nas nossas aplicações.
-> No meio temos os testes de integração, a ideia deles é verificar se um conjunto de unidades se comporta da maneira correta.
-> Na base temos os testes de unidade, onde verificamos o funcionamento da menor unidade de código testável da nossa aplicação.


