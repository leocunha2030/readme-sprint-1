-----------------------------------------------------------------------------------
                --> Dia 7 e Dia 8<--
------------------------------------------------------------------------------------
--> Revisão de Java
------------------------------------------------------------------------------------

--> Revisei alguns conceitos basicos da linguagem Java, tais como:

->Variáveis: São usadas para armazenar dados na memória.

->Tipos de Dados Primitivos: São tipos básicos como int, double, boolean, char.

->Classes e Objetos: Classes definem estrutura e comportamento, objetos são instâncias de classes.

->Métodos: Blocos de código que realizam tarefas específicas.

->Estruturas de Controle: Controlam o fluxo de execução (if-else, loops).

->Arrays: Armazenam elementos do mesmo tipo.

->Herança: Permite que uma classe herde características de outra.

->Interfaces: Definem métodos que uma classe deve implementar.

->Exceções: Lidam com erros e exceções inesperadas.

->Pacotes: Organizam classes em grupos lógicos.


Além de revisitar um curso, o qual eu fazia a algum tempo, assisti alguns modulos de Programação orientada a objetos e estou realizando exercicios de fixação para aprimorar meu aprendizado.

<h1>![java](https://github.com/leocunha2030/Projeto-Git/assets/134326959/b936155c-4090-42c0-9e54-9f2a6ca49401)
</h1>