-------------------------------------------------------------------------------------
                --> Dia 3<--
---------------------------------------------------------------------------------------
---->A respeito do Scrum aprendi que:<----
--------------------------------------------------------------------------------------

-->Scrum é um framework ágil para o desenvolvimento de projetos.

-->Scrum utiliza ciclos de desenvolvimento curtos e frequentes chamados de Sprints.

-->Sprint Planning é uma reunião realizada no início de cada Sprint.

-->Na Sprint Planning, a equipe e o Product Owner definem os itens do backlog a serem trabalhados na Sprint.

-->Scrum enfatiza a melhoria contínua do processo de desenvolvimento.

-->Scrum é baseado em princípios de transparência, inspeção e adaptação.

-->Scrum Master, Product Owner e equipe de desenvolvimento são os papéis no Scrum.

-->Scrum promove colaboração, comunicação frequente e entrega contínua de valor.

-->Scrum permite adaptação às mudanças e prioridades ao longo do projeto.

---------------------------------------------------------------------------------------
---->A respeito da Cultura Ágil aprendi que:<----
---------------------------------------------------------------------------------------
-->Cultura ágil enfatiza colaboração, flexibilidade e entrega de valor contínua.

-->Prioriza a satisfação do cliente e a entrega de software funcional em incrementos frequentes.

-->Valoriza a transparência, comunicação aberta e frequente entre a equipe e as partes interessadas.

-->Promove trabalho em equipe autogerenciada e tomada de decisões colaborativa.

-->Busca a melhoria contínua por meio de experimentação e soluções inovadoras.

-->Foca na adaptação às mudanças e resiliência durante o desenvolvimento.

-->Estimula a confiança, autonomia e responsabilidade das equipes.

-->Equilibra o planejamento com a capacidade de responder rapidamente a mudanças e prioridades.


-------------------------------------------------------------------------------------------
Neste dia aprimorei meus conhecimentos sobre Scrum e suas funcionalidades e estou me familiarizando com os conceitos da cultura ágil e as responsabilidades de um QA.