-----------------------------------------------------------------------------------------------
                --> Dia 2<--
------------------------------------------------------------------------------------------------

Aprendi os pilares fundamentais do Scrum, os quais são:

->Transparência 

->Inspeção 

->Adaptação

--------------------------------------------------------------------------------------------------

E os papeis e suas funções:

->Scrum Master---- É quem possui um grande conhecimento de Scrum responsável por auxiliar e ajudar o time

->Product Owner----É quem representa o cliente, responsável por definir as necessidades e prioridades

->Time de desenvolvimento----É a equipe que vai desenvolver as funcionalidades do produto, de forma autômona e    organizadada colaborando para atender as necessidades exigidas da melhor forma.

--------------------------------------------------------------------------------------------------


Aprendi também sobre os 4 valores e os 12 princípios do Manifesto Ágil.



