--------------------------------------------------------------------------------------
                -->Dia 4<--
---------------------------------------------------------------------------------------
---->Os objetivos da testagem de qualidade e automação na programação são:<----
---------------------------------------------------------------------------------------

->Detecção de defeitos: Identificar erros, falhas e comportamentos inesperados no software.

->Validação das funcionalidades: Verificar se as funcionalidades do software estão funcionando conforme o esperado.

->Melhoria da qualidade: Através da identificação e correção de defeitos, o software se torna mais confiável, estável e seguro.

->Redução de riscos: Mitigar problemas graves no software, evitando falhas em produção, violações de segurança e insatisfação dos usuários.

->Eficiência no desenvolvimento: Automatizar os testes para acelerar o processo de validação do software, permitindo ciclos de desenvolvimento mais curtos e entrega ágil.



----------------------------------------------------------------------------------------
---->A adoção de testes de qualidade e automação na programação gera os seguintes benefícios no software desenvolvido:<----
----------------------------------------------------------------------------------------

->Confiabilidade: O software passa a ser mais confiável, uma vez que os testes identificam e corrigem defeitos, reduzindo a probabilidade de falhas ou comportamentos inesperados.

->Estabilidade: Com a detecção e correção de defeitos, o software se torna mais estável, minimizando travamentos, erros graves e interrupções indesejadas.

->Segurança: A testagem de qualidade ajuda a identificar vulnerabilidades e falhas de segurança, permitindo que sejam corrigidas antes do lançamento do software, garantindo assim uma maior proteção aos dados e informações dos usuários.

->Satisfação do usuário: Um software testado e livre de defeitos proporciona uma melhor experiência do usuário, aumentando a satisfação e a confiança dos clientes no produto.

->Menor risco de retrabalho: Ao identificar e corrigir defeitos durante as fases iniciais do desenvolvimento, evita-se retrabalho e custos adicionais no futuro, economizando tempo e recursos.

->Agilidade no desenvolvimento: A automação dos testes permite a execução rápida e repetida de cenários de teste, acelerando o processo de validação do software e permitindo ciclos de desenvolvimento mais curtos.

->Redução de custos: A detecção precoce de defeitos e a melhoria da qualidade resultam em menor necessidade de correções e manutenção pós-lançamento, o que contribui para a redução de custos no desenvolvimento e suporte do software.


-----------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------

--> Em resumo, o aprendido hoje foi que a testagem tem diversas etapas importantes e que ela é fundamental para desenvolver um programa sem falhas. Na medida em que a testagem é feita, a confiabilidade de um software aumenta, ou seja, Testar é o processo que consiste em verificar se o software desenvolvido contém falhas, e retornar as informações obtidas para a equipe responsável.


                -->Anotaçoes da reuniao do grupo 4
----------------------------------------------------------------------------------------
Como gerar qualidade  nos projetos?
Testando o software e verificando os requisitos do cliente.

-->Assuntos abordados na reunião:

Erro na amazon permite acumular cupons de desconto causando prejuizos a empresa.
"De acordo com os consumidores, a Amazon logo suspendeu a validade dos cupons cumulativos e não permite mais que novos clientes vinculem vouchers para obter até 100% de desconto sobre o valor dos produtos."

Este erro poderia ter sido evitado pela testagem de qualidade que encontraria o erro e o reportaria para a equipe de desenvolvedores
Alguns dos fundamentos do teste de qualidade que evitariam este erro são: Identificar defeitos, reduzir riscos e confiança do software.

Além dos casos do Ifood e do foguete Ariane 501 Flight(Erro de ponto flutuante causa prejuizo de meio bilhão de dolares)


Estes foram os assuntos tratados na reunião do grupo 4.

https://olhardigital.com.br/2022/01/26/pro/possivel-bug-na-amazon-faz-consumidores-comprarem-quase-de-graca/

https://itsfoss.com/a-floating-point-error-that-caused-a-damage-worth-half-a-billion/

https://tecnoblog.net/noticias/2018/09/17/ifood-falha-cupons/